# Terrain Slicer

Two scripts for the generation of mesh tiles and map tiles.

#### TODOs

* Support offset
* Set paths to executables by user

## Slice Mesh

Slice a given mesh every interval unit along X and Z axes to generate equal-sized tiles.
Every tile has new UV coordinates ranging [0,1].
Only OBJ file that contains a single mesh is currently supported.

    node -r esm slice-mesh.js [OPTIONS]

#### Options

|Flag|Default|Description|
|---|---|---|
|`-i`, `--input`|_(Required)_|Path to OBJ file|
|`-o`, `--output`|`'output'`|Path to output folder|
|`--interval`|`256`|Interval units|
|`-k`, `--keepFiles`|`false`|Keep generated OBJ and DAE files|
|`-h`, `--help`||Show help|

#### Requirements

* ASSIMP for optimizing meshes and converting to DAE
* COLLADA2GLTF converter with Draco compression

## Slice Map

Slice a map/texture into tiles of equal size that fits on the tiled mesh.
A map requires a metafile (see example below) that holds information how this map is positioned, i.e. world position for each map corner.

    node -r esm slice-map.js [OPTIONS]

#### Options

|Flag|Default|Description|
|---|---|---|
|`-i`, `--input`|_(Required)_|Path to map/texture file|
|`-o`, `--output`|`'output'`|Path to output folder|
|`-m`, `--metafile`|_(Required)_|Path to JSON metafile|
|`--tileSize`|`256`|Size of tiles of sliced mesh (see interval units)|
|`--mapSize`|`256`|Size of output map tiles in pixels|
|`-c`, `--color`|`'#808080'`|Background color|
|`-h`, `--help`||Show help|

#### Requirements

* ImageMagick for image processing

#### Example meta.json

```
{
    "filename": "1967_{index}.jpg",
    "width": 6829,
    "height": 6846,
    "tileSize": 1024,
    "top-left": {
        "x": -295.125,
        "y": 0,
        "z": -1630.944
    },
    "top-right": {
        "x": 2192.228,
        "y": 0,
        "z": -1591.869
    },
    "bottom-right": {
        "x": 2153.057,
        "y": 0,
        "z": 901.569
    },
    "bottom-left": {
        "x": -334.295,
        "y": 0,
        "z": 862.494
    }
}
```
