import { Line3, Vector3 } from 'three';
import { round } from './utils';

export class Face {

  /**
   * @type {Vector3}
   */
  a;
  /**
   * @type {Vector3}
   */
  b;
  /**
   * @type {Vector3}
   */
  c;

  area = 0;

  indexX = [];
  indexZ = [];

  /**
   *
   * @param a {Vector3}
   * @param b {Vector3}
   * @param c {Vector3}
   */
  constructor(a, b, c) {

    this.a = a;
    this.b = b;
    this.c = c;

    const centroid = new Vector3().add(a).add(b).add(c).divideScalar(3);
    const cxIndex = Math.floor(centroid.x / opt.options.interval);
    const czIndex = Math.floor(centroid.z / opt.options.interval);

    [a, b, c].forEach(v => {
      const ix = v.x % opt.options.interval === 0 ? cxIndex : Math.floor(v.x / opt.options.interval);
      const iz = v.z % opt.options.interval === 0 ? czIndex : Math.floor(v.z / opt.options.interval);

      if (!this.indexX.includes(ix)) {
        this.indexX.push(ix);
      }
      if (!this.indexZ.includes(iz)) {
        this.indexZ.push(iz);
      }
    });

    this.indexX.sort((a, b) => a - b);
    this.indexZ.sort((a, b) => a - b);

    const vec1 = new Vector3().subVectors(b, a);
    const vec2 = new Vector3().subVectors(c, a);
    this.area = vec1.cross(vec2).length() / 2;
  }

  /**
   * @param plane {Plane}
   * @return {Face[]}
   */
  split(plane) {

    // get intersection points
    let vA = plane.intersectLine(new Line3(this.a, this.b), new Vector3());
    let vB = plane.intersectLine(new Line3(this.b, this.c), new Vector3());
    let vC = plane.intersectLine(new Line3(this.c, this.a), new Vector3());

    // round values and check if intersection point equals one of the face points
    if (vA) {
      vA.x = round(vA.x);
      vA.y = round(vA.y);
      vA.z = round(vA.z);

      if(this.a.equals(vA) || this.b.equals(vA)) {
        vA = undefined;
      }
    }
    if (vB) {
      vB.x = round(vB.x);
      vB.y = round(vB.y);
      vB.z = round(vB.z);

      if(this.b.equals(vB) || this.c.equals(vB)) {
        vB = undefined;
      }
    }
    if (vC) {
      vC.x = round(vC.x);
      vC.y = round(vC.y);
      vC.z = round(vC.z);

      if (this.c.equals(vC) || this.a.equals(vC)) {
        vC = undefined;
      }
    }

    const faces = [];

    // plane slices between face points -> 3 new triangles
    if (vA && vB) {

      faces.push(
        new Face(this.b, vB, vA),
        new Face(this.a, vA, vB),
        new Face(this.a, vB, this.c)
      );

    } else if (vB && vC) {

      faces.push(
        new Face(this.c, vC, vB),
        new Face(this.b, vB, vC),
        new Face(this.a, this.b, vC)
      );

    } else if (vA && vC) {

      faces.push(
        new Face(this.a, vA, vC),
        new Face(this.c, vC, vA),
        new Face(vA, this.b, this.c),
      );

    }

    if (faces.length) {
      return faces;
    }

    // one of the face points is on plane -> only 2 new triangles
    if (vA) {

      faces.push(
        new Face(this.c, this.a, vA),
        new Face(this.c, vA, this.b)
      );

    } else if (vB) {

      faces.push(
        new Face(this.a, this.b, vB),
        new Face(this.a, vB, this.c)
      );

    } else if (vC) {

      faces.push(
        new Face(this.b, this.c, vC),
        new Face(this.b, vC, this.a)
      );

    }

    if (faces.length) {
      return faces;
    }

    // still no faces -> error
    console.log(this);
    console.log(vA, vB, vC);
    throw new Error('No faces');

  }

}
