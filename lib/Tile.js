import { Box3, BufferAttribute, BufferGeometry, Vector3 } from 'three';

export class Tile {

  /**
   * @type {Box3}
   */
  boundingBox;
  /**
   * @type {number}
   */
  indexX;
  /**plane
   * @type {number}
   */
  indexZ;
  /**
   *
   * @type {Face[]}
   */
  faces = [];

  constructor(indexX, indexZ) {

    this.indexX = indexX;
    this.indexZ = indexZ;

    const interval = opt.options.interval;
    this.boundingBox = new Box3(
      new Vector3(indexX * interval, 0, indexZ * interval),
      new Vector3(indexX * interval + interval, 0, indexZ * interval + interval)
    );

  }

  /**
   * @param face {Face}
   */
  addFace(face) {

    this.faces.push(face);

  }

  /**
   * @return {BufferGeometry}
   */
  getBufferGeometry() {

    const bbSize = this.boundingBox.getSize(new Vector3());

    const vertices = [];
    const normals = [];
    const uvs = [];

    // generate texture coordinates
    this.faces.forEach(f => {
      [f.a, f.b, f.c].forEach(v => {
        vertices.push(v.x, v.y, v.z);
        const t = v.clone().sub(this.boundingBox.min);
        uvs.push(t.x / bbSize.x, t.z / bbSize.z);
        // uvs.push(t.x / bbSize.x, 1.0 - t.z / bbSize.z);
      });
    });

    const geometry = new BufferGeometry();

    geometry.setAttribute('position', new BufferAttribute(new Float32Array(vertices), 3));
    geometry.setAttribute('uv', new BufferAttribute(new Float32Array(uvs), 2));

    return geometry;

  }

}
