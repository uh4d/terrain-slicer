/**
 *
 * @param vector {Vector3}
 * @return {{x: number, z: number}}
 */
export function getTilesIndex(vector) {

  return {
    x: vector.x % opt.options.interval ? 0 : Math.floor(vector.x / opt.options.interval),
    z: vector.z % opt.options.interval ? 0 : Math.floor(vector.z / opt.options.interval)
  };

}

/**
 *
 * @param value {number}
 * @param decimals {number=}
 * @return {number}
 */
export function round(value, decimals = 3) {

  const factor = Math.pow(10, decimals);

  return Math.round((value + Number.EPSILON) * factor) / factor;

}
