import { create as createGetopt } from 'node-getopt';
import { execFile } from 'child-process-promise';
import { SingleBar, Presets } from 'cli-progress';
import * as fs from 'fs-extra';
import * as path from 'path';
import { Box2, MathUtils, Vector2 } from 'three';

const getopt = createGetopt([
  ['i', 'input=ARG', 'Input file'],
  ['o', 'output=ARG', 'Output path', 'output'],
  ['m', 'metafile=ARG', 'Meta file'],
  ['', 'tileSize=ARG', 'Tile size', 256],
  ['', 'mapSize=ARG', 'Map size', 256],
  ['c', 'color=ARG', 'Background color', '#808080'],
  ['h', 'help']
]).bindHelp(`
Usage: node -r esm slice-map.js [OPTIONS]
[[OPTIONS]]
`);
global.opt = getopt.parseSystem();

// check for required args
if (!opt.options.input) {
  console.error('No input file set!');
  getopt.showHelp();
  process.exit(1);
}
if (opt.options.metafile) {
  console.error('No meta file set!');
  getopt.showHelp();
  process.exit(2);
}

const progressBar = new SingleBar({}, Presets.shades_classic);

main();

async function main() {

  // read extents of map
  const meta = fs.readJSONSync(opt.options.metafile);

  const tl = new Vector2(meta['top-left'].x, meta['top-left'].z);
  const tr = new Vector2(meta['top-right'].x, meta['top-right'].z);
  const br = new Vector2(meta['bottom-right'].x, meta['bottom-right'].z);
  const bl = new Vector2(meta['bottom-left'].x, meta['bottom-left'].z);

  // bounding box in terrain units
  const bbox = new Box2()
    .expandByPoint(tl)
    .expandByPoint(tr)
    .expandByPoint(br)
    .expandByPoint(bl);

  // cli options mapSize
  const mapSize = parseInt(opt.options.mapSize);
  const pixelFactor = mapSize / parseInt(opt.options.tileSize);

  // size in pixels
  const size = bbox.getSize(new Vector2()).multiplyScalar(pixelFactor).round();

  // extents in pixels
  const pxMin = bbox.min.clone().multiplyScalar(pixelFactor).round();
  const pxMax = bbox.max.clone().multiplyScalar(pixelFactor).round();

  // compute rotation angle
  const angle = new Vector2().subVectors(tr, tl).angle() * MathUtils.RAD2DEG;
  console.log('Angle:', angle);

  await fs.ensureDir(opt.options.output);

  const tempFile = path.join(opt.options.output, 'temp.jpg');

  // rotate image with ImageMagick
  await execFile('magick', [opt.options.input, '-background', opt.options.color, '-rotate', angle, '-resize', `${size.x}x${size.y}!`, tempFile]);

  const extents = {
    min: {
      x: Math.floor(pxMin.x / mapSize),
      y: Math.floor(pxMin.y / mapSize)
    },
    max: {
      x: Math.ceil((size.x + pxMin.x) / mapSize) - 1,
      y: Math.ceil((size.y + pxMin.y) / mapSize) - 1
    },
  };

  progressBar.start((extents.max.x - extents.min.x + 1) * (extents.max.y - extents.min.y + 1), 0);

  // iterate over tiles
  for (let iy = extents.min.y, ly = extents.max.y + 1; iy < ly; iy++) {
    for (let ix = extents.min.x, lx = extents.max.x + 1; ix < lx; ix++) {

      const tileMin = new Vector2(ix * mapSize, iy * mapSize);
      // const tileMax = new Vector2((ix + 1) * mapSize, (iy + 1) * mapSize);

      const offset = tileMin.clone().sub(pxMin);
      const crop = `${mapSize}x${mapSize}${offset.x < 0 ? offset.x : '+' + offset.x}${offset.y < 0 ? offset.y : '+' + offset.y}!`;

      await execFile('magick', ['-size', `${mapSize}x${mapSize}`, 'canvas:' + opt.options.color, '(', tempFile, '-crop', crop, '+repage', ')', '-geometry', `+${offset.x < 0 ? -offset.x : 0}+${offset.y < 0 ? -offset.y : 0}`, '-compose', 'Over', '-composite', '-quality', 60, path.join(opt.options.output, `tile_${ix}_${iy}.jpg`)]);

      progressBar.increment();

    }
  }

  progressBar.stop();

  // write meta file
  fs.writeJSONSync(path.join(opt.options.output, 'meta.json'), {
    tileSize: opt.options.tileSize,
    mapSize,
    extents,
    baseColor: opt.options.color
  }, {spaces: 2});

  // remove rotated temp image
  fs.removeSync(tempFile);

}
