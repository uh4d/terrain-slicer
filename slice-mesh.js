import { create as createGetopt } from 'node-getopt';
import fs from 'fs-extra';
import * as path from 'path';
import { execFile } from 'child-process-promise';
import { SingleBar, Presets} from 'cli-progress';
import { OBJLoader2 } from 'three/examples/jsm/loaders/OBJLoader2';
import {
  Group,
  Mesh,
  MeshBasicMaterial,
  Plane,
  Vector3
} from 'three';
import { Face } from './lib/Face';
import { Tile } from './lib/Tile';
import { OBJExporter } from './lib/OBJExporter';
import { round } from './lib/utils';

const getopt = createGetopt([
  ['i', 'input=ARG', 'Input file'],
  ['o', 'output=ARG', 'Output path', 'output'],
  ['', 'interval', 'Interval units', 256],
  ['k', 'keepFiles', 'Keep generated OBJ and DAE files', false],
  ['h', 'help']
]).bindHelp(`
Usage: node -r esm slice-mesh.js [OPTIONS]
[[OPTIONS]]
`);
global.opt = getopt.parseSystem();

// check for required args
if (!opt.options.input) {
  console.error('No input file set!');
  getopt.showHelp();
  process.exit(1);
}

const extname = path.extname(opt.options.input);

// check for OBJ format
if (extname !== '.obj' && extname !== '.OBJ') {
  console.warn('Input file is not OBJ format!');
  process.exit(2);
}

const progressBar = new SingleBar({}, Presets.shades_classic);

/**
 *
 * @type {Map<string, Tile>}
 */
const tilesMap = new Map();

main();

async function main() {

  console.log('File:', opt.options.input);

  // load mesh from file
  const xmlString = await fs.readFile(opt.options.input);

  const loader = new OBJLoader2();
  const obj = loader.parse(xmlString);
  const mesh = obj.children[0];

  await splitMesh(mesh.geometry);

  await exportTiles();

  console.log('Done');

}

/**
 *
 * @param geometry {BufferGeometry}
 */
async function splitMesh(geometry) {

  const positionAttr = geometry.getAttribute('position');

  console.log('Vertices:', positionAttr.count);
  console.log('Faces:', positionAttr.count / 3);

  console.log('Split mesh...');

  progressBar.start(positionAttr.count / 3, 0);

  for (let i = 0, l = positionAttr.count; i < l; i += 3) {

    const a = new Vector3(round(positionAttr.getX(i)), round(positionAttr.getY(i)), round(positionAttr.getZ(i)));
    const b = new Vector3(round(positionAttr.getX(i + 1)), round(positionAttr.getY(i + 1)), round(positionAttr.getZ(i + 1)));
    const c = new Vector3(round(positionAttr.getX(i + 2)), round(positionAttr.getY(i + 2)), round(positionAttr.getZ(i + 2)));

    /**
     * @type {Face[]}
     */
    const faces = [new Face(a, b, c)];

    let iteration = 0;

    // check if faces need to be split
    for (let j = 0; j < faces.length; j++) {
      const f = faces[j];

      if (f.area === 0) {
        faces.splice(j, 1);
        j--;
        continue;
      }

      let slicePlane;

      if (f.indexX.length > 1) {
        slicePlane = new Plane(new Vector3(-1, 0, 0), f.indexX[f.indexX.length - 1] * opt.options.interval);
      } else if (f.indexZ.length > 1) {
        slicePlane = new Plane(new Vector3(0, 0, -1), f.indexZ[f.indexZ.length - 1] * opt.options.interval);
      }

      if (slicePlane) {
        faces.splice(j, 1, ...f.split(slicePlane));
        iteration++;
        if (faces.length > 9 || iteration > 9) {
          console.log(faces);
          console.log(a, b, c);
          process.exit();
        }
        j--;
      }
    }

    // put faces to tiles map
    faces.forEach(f => {
      const key = `${f.indexX[0]}/${f.indexZ[0]}`;
      if (!tilesMap.has(key)) {
        tilesMap.set(key, new Tile(f.indexX[0], f.indexZ[0]));
      }
      tilesMap.get(key).addFace(f);
    });

    progressBar.increment();

  }

  progressBar.stop();

}

async function exportTiles() {

  console.log('Process tiles...');

  const exporter = new OBJExporter();
  const tempMat = new MeshBasicMaterial();
  const group = new Group();

  for (const [key, tile] of tilesMap) {

    const mesh = new Mesh(tile.getBufferGeometry(), tempMat);
    mesh.name = `tile_${tile.indexX}_${tile.indexZ}`;
    group.add(mesh);
  }

  const output = exporter.parse(group);

  // const filename = `tile_${tile.indexX}_${tile.indexZ}`;
  const filename = path.basename(opt.options.input, extname) + '_tiled';
  const objFile = path.join(opt.options.output, filename + '.obj');
  const daeFile = path.join(opt.options.output, filename + '.dae');
  const gltfFile = path.join(opt.options.output, filename + '.gltf');

  await fs.ensureDir(opt.options.output);

  // write obj
  await fs.writeFile(objFile, output);

  // convert to collada
  await execFile('C:\\ServerTools\\assimp-4.0.1\\assimp.exe', ['export', objFile, daeFile, '-jiv', '-om']);

  // convert to gltf
  await execFile('C:\\ServerTools\\COLLADA2GLTF-v2.1.4-x86\\COLLADA2GLTF-bin.exe', ['-i', daeFile, '-o', gltfFile, '--preserveUnusedSemantics', '-d', '--qp', 12, '--qn', 7, '--qt', 8]);

  // remove obj and dae
  if (!opt.options.keepFiles) {
    await fs.remove(objFile);
    await fs.remove(daeFile);
  }

}

