import * as utm from 'utm';
import { Matrix4, Quaternion, Vector3 } from 'three';

// Example matrix ( http://4dbrowser.urbanhistory4d.org/#!/search?query=df_pos-2012-c_0000023 )
const rawMatrix = [
  -0.59811053049099, -2.7755575615628914e-17, 0.8014136218681257, 0,
  -0.17680889874214, 0.9753595495966924, -0.13195591057671166, 0,
  -0.7816664292659491, -0.22062127959591168, -0.5833728176287307, 0,
  177.64420716088446, 4.824870892888283, -601.8950240983784, 1
];

const matrix = new Matrix4().fromArray(rawMatrix);

// extract translation and rotation
const translation = new Vector3().setFromMatrixPosition(matrix);
const rotation = new Quaternion().setFromRotationMatrix(matrix);

console.log('Translation:', translation);
// Translation: Vector3 {
//   x: 177.64420716088446,
//   y: 4.824870892888283,
//   z: -601.8950240983784
// }


// UTM offset of local coordinate system in UH4D browser
const utmOffset = new Vector3(410973.906, 107.392, 5655871.0);
const utmZone = 33;

// apply offset,
const utmPosition = translation.clone()
  .multiply(new Vector3(1, 1, -1))
  .add(utmOffset);

console.log('UTM Coordinates:', utmPosition);
// UTM Coordinates: Vector3 {
//   x: 411151.5502071609,  -> easting
//   y: 112.21687089288828, -> height
//   z: 5656472.895024098   -> northing
// }

// convert to LatLon
console.log('LatLon:', utm.toLatLon(utmPosition.x, utmPosition.z, utmZone, undefined, true));
// LatLon: { latitude: 51.05290748931661, longitude: 13.73234034175551 }

console.log('Camera negative Z normal:', new Vector3(0, 0, -1).applyQuaternion(rotation));
// Camera negative Z normal: Vector3 {
//   x: 0.7816664292659489,
//   y: 0.22062127959591166,
//   z: 0.5833728176287306
// }

